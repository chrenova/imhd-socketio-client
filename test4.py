import asyncio
from random import randint

async def coro():
    i = randint(0, 100)
    while True:
        print(i)
        i += 1
        await asyncio.sleep(1)

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.create_task(coro())
    loop.create_task(coro())
    loop.run_forever()
