# import requests
from socketIO_client import SocketIO, LoggingNamespace
import logging

'''
depends on a fork of socketio client,
https://github.com/invisibleroads/socketIO-client/issues/129
'''


# logging.getLogger('socketIO-client').setLevel(logging.DEBUG)
# logging.basicConfig()

class Runner():
    def __init__(self):
        self.socketIO = None

    def connect(self):
        try:
            self.socketIO = SocketIO('https://m.imhd.sk', 443, resource='/rt/sio2/', verify=False, Namespace=LoggingNamespace)
            #self.socketIO = SocketIO('m.imhd.sk', 443, resource='/rt/sio2/', verify=False, Namespace=LoggingNamespace)
        except Exception as e:
            print('Failed to connect: ' + e)
        print(self.socketIO)
        print(self.socketIO.connected)

    def run(self, produce, req):

        def on_connect(*body):
            print('connect: {}'.format(body))

        def on_disconnect():
            print('disconnect')
            # TODO: if stopped by user, then do nothing; else reconnect?
            # self.connect()
            # self.run(produce, req)

        def on_reconnect():
            print('reconnect')

        def on_tabs(*body):
            print('len(tabs): {}'.format(len(body)))
            # print('tabs: {}'.format(body))
            data = body[0]
            '''
            for k, v in data.items():
              #print(k)
              v_props = ['timestamp', 'zastavka', 'nastupiste', 'tab']
              tab_props = ['lastZ', 'casDelta', 'issi', 'linka', 'cielZastavka', 'cas', 'cielCiel', 'typ']
              #typ = 'online' | 'cp'
            '''
            produce(data)

        def on_vinfo(*body):
            print('vInfo: {}'.format(body))

        # note: event parameter is request method, here get for GET
        # second parameter is emit_data structured as described above
        # socketIO.on('connect', on_connect)
        self.socketIO.on('connect', on_connect)
        self.socketIO.on('disconnect', on_disconnect)
        self.socketIO.on('reconnect', on_reconnect)
        self.socketIO.on('tabs', on_tabs)
        self.socketIO.on('vInfo', on_vinfo)
        #self.socketIO.emit('req', req, on_connect)
        self.socketIO.emit('tabStart', req, on_connect)
        # adjust with your requirements
        self.socketIO.wait_for_callbacks()
        print('run() finished')

    def stop(self):
        self.socketIO.disconnect()


'''
vInfo: ({'np': 0, 'ac': 0, 'img': 797, 'imgt': 1, 'typ': 'ČKD Tatra T6', 'suprava': '7904+7903', 'issi': '7904'},)
vInfo: ({'np': 1, 'ac': 1, 'img': 740, 'imgt': 0, 'typ': 'Škoda 30T ForCity Plus', 'issi': '7518'},)


tabs: ({'332.4': {'zastavka': 332, 'nastupiste': 4, 'timestamp': 1542740636469, 'tab': 
  [{'linka': '209', 'issi': 6031, 'cielCiel': 580, 'cielZastavka': '225', 'cas': 1542740700000, 'casDelta': 2, 'typ': 'online', 'lastZ': 144}, 
   {'linka': '64', 'issi': 6022, 'cielCiel': 235, 'cielZastavka': '128', 'cas': 1542740760000, 'casDelta': 0, 'typ': 'online', 'lastZ': 306}, 
   {'linka': '61', 'issi': 1862, 'cielCiel': 365, 'cielZas'



https://m.imhd.sk/ba/api/sk/cp?op=gsn&id=351,475,276,229,426,1,32,61,215,127,310,451,189,516,357,178,93,95,390,462,3,330,177,367,416,334,198,144,572,332,183,471,252,482,5,175,452,361,392,481,545,410&ts=1542744457219
https://m.imhd.sk/ba/api/sk/cp?op=gdn&id=760,495,670,60,174,567,230,107,119,700,365,533,70,340,150,540,740,597,151,400,751,350,753,470,330,705,630,770,214,105&ts=1542744457224

'''

if __name__ == '__main__':
    runner = Runner()
    runner.connect()
    def f(data):
        print(data)
    runner.run(f, [144, ['*']])
