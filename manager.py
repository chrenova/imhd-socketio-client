import asyncio
import janus
import logging
from concurrent.futures import CancelledError
from test3 import Runner
from functools import partial

#logging.basicConfig(level=logging.INFO)

def produce(data, q):
    q.put(data)

async def consume(q):
    print('Starting consumer')
    while True:
        val = await q.get()
        print(val)
        q.task_done()

def run(inst, q, req):
    inst.run(partial(produce, q=q), req)

#async def main(loop, runner):
#    await loop.run_in_executor(None, run, runner)
#    print('Done')

def stop(inst):
    inst.stop()

async def timeout(sec):
    await asyncio.sleep(sec)


async def poll(incr=1):
    i = 0
    while True:
        print(f'Polling at {i} seconds.')
        i += incr
        await asyncio.sleep(incr)

async def kill(duration):
    await asyncio.sleep(duration)

async def kill2(duration, runner):
    await asyncio.sleep(duration)
    runner.stop()


async def create_new_task(loop, queue, runner, req):
    try:
        runner.connect()
    except Exception as e:
        print(e)

    '''
    tasks = [asyncio.ensure_future(poll(2), loop=loop),
             asyncio.ensure_future(poll(1.4), loop=loop),
             asyncio.ensure_future(kill(5), loop=loop)]
    '''
    await loop.run_in_executor(None, run, runner, queue.sync_q, req)
    print('create_new_task finished')

    '''
    tasks = [loop.run_in_executor(None, run, runner, queue.sync_q, req),
             asyncio.ensure_future(consume(queue.async_q), loop=loop),
             asyncio.ensure_future(kill2(5, runner), loop=loop)]


    finished, pending = loop.run_until_complete(
        asyncio.wait(tasks, return_when=asyncio.FIRST_COMPLETED))
    logging.debug(">> Finished: {}", finished)
    logging.debug(">> Pending: {}", pending)
    # Cancel the remaining tasks
    for task in pending:
        logging.info("Cancelling %s: %s", task, task.cancel())
    try:
        loop.run_until_complete(asyncio.gather(*pending))
    except CancelledError: # Any other exception would be bad
        for task in pending:
            logging.info("Cancelled %s: %s", task, task.cancelled())
    print('create_task finished')
    '''

async def main(loop, req):
    print('main starting')
    await create_new_task(loop, req)
    print('main finished')


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    queue = janus.Queue(loop=loop)
    consumer_task = loop.create_task(consume(queue.async_q))
    runner1 = Runner()
    runner2 = Runner()
    task1 = loop.create_task(create_new_task(loop, queue, runner1, [332, ['*']]))
    task2 = loop.create_task(create_new_task(loop, queue, runner2, [144, ['*']]))
    #loop.create_task(kill2(5, runner2))

    try:
        loop.run_forever()
    finally:
        loop.run_until_complete(loop.shutdown_asyncgens())
        loop.close()

    '''
  runner = Runner()
  runner.connect()
  #asyncio.ensure_future(main(loop, runner))
  runner_future = loop.run_in_executor(None, run, runner, queue.sync_q)
  consume_future = consume(queue.async_q)
  t = asyncio.ensure_future(consume_future, loop=loop)
  #loop.run_until_complete(timeout(5))
  #asyncio.ensure_future(timeout(5), loop=loop)
  #loop.create_task(timeout(5))
  #loop.run_in_executor(None, stop, runner)
  #runner.stop()
  #t.cancel()
  #await t
  loop.run_until_complete(runner_future)
  print('Done')
  print(f'runner_future: {runner_future}')
  print(f'consume_future: {consume_future}')
    '''

'''
https://hackernoon.com/threaded-asynchronous-magic-and-how-to-wield-it-bba9ed602c32
'''
